FROM php:7.0-fpm 
ENV REFRESHED_AT 2016-01-17

RUN apt-get update && apt-get install -y libmcrypt-dev  
RUN docker-php-ext-install mbstring pdo_mysql tokenizer


COPY buildfiles/php-fpm/php.ini /usr/local/etc/php/

ADD . /var/www/html

RUN usermod -u 1000 www-data

CMD ["php-fpm"]  

EXPOSE 9000